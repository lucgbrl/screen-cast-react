import {useState} from 'react';

import Nav from './components/Nav'
import Counter from './components/Counter'

function App() {
  
  let [counter, deadpool] = useState(0)

  return (    
    <div className="App">
      <header className="App-header">        
        <Nav user="Lucas" />
        <Counter contador={counter} dois={true} />
        <Counter contador={counter} />
        <Counter contador={counter} />
        <Counter contador={counter} />
        <button onClick={()=>{deadpool(counter+1)}}>Contador</button>
      </header>
    </div>
  );
}

export default App;
