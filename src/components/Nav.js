function Nav(props) {
    return(
        <>
            <nav>
                <h1>Logo</h1>
                {
                    props.user 
                    ? <span>{props.user.toUpperCase()}</span>
                    : <button>Login</button>             
                }
            </nav>
        </>
    )
}

export default Nav 